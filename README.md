# LET'S MEET Keycloak Theme

This is a keycloak theme created for LET'S MEET project. The theme has Fuse theme look and feel

## Screentshots

### Login page

#### on large screens

![login-page-large-screen](./Theme-screenshots/login-page-large-screen.png)

#### on small screens

![login-page-small-screen](./Theme-screenshots/login-page-small-screen.png)

### Resgistration page

#### on large screens

![register-page-large-screen](./Theme-screenshots/register-page-large-screen.png)

#### on small screens

![register-page-small-screen](./Theme-screenshots/register-page-small-screen.png)

## Setup

-   Copy LetsMeet-keycloak-theme folder to the themes folder under the Keycloak folder
-   Enter to Keycloak Administration Console and log in
-   Choose your client
-   On the client configuaration page, change login theme option to LetsMeet-keycloak-theme
-   Save the changes

## Copyright

This theme was created by Chady  
Ecocloud, Inc. All rights reserved.
