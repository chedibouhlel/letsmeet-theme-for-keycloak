<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <title><#nested "title"></title>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <link href="${url.resourcesPath}/img/favicon.ico" rel="icon"/>
</head>

	<body>
        <#nested "header">
        <div class="login-content" style="background-image: url(&quot;${url.resourcesPath}/img/dark-material-bg.jpg&quot;);">
            <div class="login-intro">
                <div class="logo">
                    <img src="${url.resourcesPath}/img/LetsMeet.svg" />
                </div>
    
                <div
                    class="title"
                >
                    Welcome to LET'S MEET!
                </div>
    
                <div
                    class="description"
                >
                    Let's Meet is a cloud-based video communications app that allows you
                    to set up virtual video and audio conferencing, webinars, live
                    chats, screen-sharing, and other collaborative capabilities.
                </div>
            </div>
            <div class="box">
                <div>
                    <img class="logo" src="${url.resourcesPath}/img/LetsMeet.svg" alt="LET'S MEET">
                </div>
                <#nested "form-header">
                <#if displayMessage && message?has_content>
                    <div class="alert alert-${message.type}">
                        <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                        <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                        <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                        <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                        <span class="message-text">${message.summary?no_esc}</span>
                    </div>
                </#if>
                <#nested "form">
                <#nested "form-footer">
                <div>
                    <p class="copyright">&copy; ${msg("copyright", "${.now?string('yyyy')}")}</p>
                </div>
            </div> 
        </div>
	</body>
</html>
</#macro>
