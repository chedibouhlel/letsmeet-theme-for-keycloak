<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("registerWithTitle", "Let's Meet")}
    <#elseif section = "header">
        <!-- Compiled and minified CSS -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"/>
        <script>
            function togglePassword() {
                var x = document.getElementById("password");
                var v = document.getElementById("vi");
                if (x.type === "password") {
                    x.type = "text";
                    v.src = "${url.resourcesPath}/img/eye.png";
                } else {
                    x.type = "password";
                    v.src = "${url.resourcesPath}/img/eye-off.png";
                }
            }
        </script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <#elseif section = "form-header">
        <div>
            <p class="application-name">CREATE AN ACCOUNT</p>
        </div>
    <#elseif section = "form">
        <div class="box-container">
            <form id="kc-register-form" class="${properties.kcFormClass!} form" action="${url.registrationAction}" method="post">
                <input type="text" id="firstName" class="login-field" name="firstName" value="${(register.formData.firstName!'')}" placeholder="${msg("firstName")}" />
                <input type="text" id="lastName" class="login-field" name="lastName" value="${(register.formData.lastName!'')}" placeholder="${msg("lastName")}" />
                <input type="email" id="email" class="login-field" name="email" value="${(register.formData.email!'')}" autocomplete="email" placeholder="${msg("email")}"/>
                <#if !realm.registrationEmailAsUsername>
                    <input type="text" id="username" class="login-field" name="username" value="${(register.formData.username!'')}" autocomplete="username" placeholder="${msg("username")}"/>
                </#if>
                <#if passwordRequired??>
                    <input type="password" id="password" class="login-field" name="password" autocomplete="new-password" placeholder="${msg("password")}" />
                    <input type="password" id="password-confirm" class="login-field" name="password-confirm" placeholder="${msg("passwordConfirm")}" />
                </#if>

                <#if recaptchaRequired??>
                    <div class="form-group">
                        <div class="${properties.kcInputWrapperClass!}">
                            <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                        </div>
                    </div>
                </#if>
                <input class="submit" type="submit" value="${msg("doRegister")}"/>
            </form>
        </div>
    <#elseif section = "form-footer">
        <div class="form-footer">
            <div>Already have an account?</div>
            <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
        </div>
        
    </#if>

</@layout.registrationLayout>
