<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("pageExpiredTitle")}
    <#elseif section = "form-header">
        <div>
            <p class="application-name">${msg("pageExpiredTitle")}</p>
        </div>
    <#elseif section = "form-footer">
        <div class="form-footer">
            <div>
                ${msg("pageExpiredMsg1")}
            </div>
            <span>
                <a id="loginRestartLink" href="${url.loginRestartFlowUrl}">${msg("doClickHere")}</a>
            </span>
        </div>
    </#if>
</@layout.registrationLayout>
